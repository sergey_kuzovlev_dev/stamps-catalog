import { StampTC } from '../models/stamp';

const StampQuery = {
  stampById: StampTC.getResolver('findById'),
  stampCount: StampTC.getResolver('count'),
  stampPagination: StampTC.getResolver('pagination'),
};

const StampMutation = {
  stampCreateOne: StampTC.getResolver('createOne'),
  stampCreateMany: StampTC.getResolver('createMany'),
  stampUpdateById: StampTC.getResolver('updateById'),
  stampUpdateOne: StampTC.getResolver('updateOne'),
  stampUpdateMany: StampTC.getResolver('updateMany'),
  stampRemoveById: StampTC.getResolver('removeById'),
  stampRemoveOne: StampTC.getResolver('removeOne'),
  stampRemoveMany: StampTC.getResolver('removeMany'),
};

export { StampQuery, StampMutation };