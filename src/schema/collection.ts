import { CollectonTC } from '../models/collection';

const CollectionQuery = {
  collectonMany: CollectonTC.getResolver('findMany'),
  collectonCount: CollectonTC.getResolver('count'),
};

const CollectionMutation = {
  collectonCreateOne: CollectonTC.getResolver('createOne'),
  collectonCreateMany: CollectonTC.getResolver('createMany'),
  collectonUpdateById: CollectonTC.getResolver('updateById'),
  collectonUpdateOne: CollectonTC.getResolver('updateOne'),
  collectonUpdateMany: CollectonTC.getResolver('updateMany'),
  collectonRemoveById: CollectonTC.getResolver('removeById'),
  collectonRemoveOne: CollectonTC.getResolver('removeOne'),
  collectonRemoveMany: CollectonTC.getResolver('removeMany'),
};

export { CollectionQuery, CollectionMutation };