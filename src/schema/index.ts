import { SchemaComposer } from 'graphql-compose';
import { UserQuery, UserMutation } from './user';
import { StampQuery, StampMutation } from './stamp';
import { CurrencyQuery, CurrencyMutation } from './currency';
import { CollectionQuery, CollectionMutation } from './collection';

const schemaComposer = new SchemaComposer();

schemaComposer.Query.addFields({
  ...UserQuery,
  ...StampQuery,
  ...CurrencyQuery,
  ...CollectionQuery,
});

schemaComposer.Mutation.addFields({
  ...UserMutation,
  ...StampMutation,
  ...CurrencyMutation,
  ...CollectionMutation,
});

export default schemaComposer.buildSchema();