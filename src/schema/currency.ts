import { CurrencyTC } from '../models/currency';

const CurrencyQuery = {
  currencyById: CurrencyTC.getResolver('findById'),
};

const CurrencyMutation = {
  currencyCreateOne: CurrencyTC.getResolver('createOne'),
  currencyUpdateById: CurrencyTC.getResolver('updateById'),
  currencyRemoveById: CurrencyTC.getResolver('removeById'),

};

export { CurrencyQuery, CurrencyMutation };