import * as dotenv from "dotenv";
import express from "express";
import server from "./server";
import mongoose from 'mongoose';

import connection from './utils/db';

dotenv.config();

const app = express();
const port = process.env.PORT;
const graphqlServer = server();

graphqlServer.applyMiddleware({
  app,
  path: '/',
  cors: true
});

app.listen({ port }, async () => {
  await connection;
  if(mongoose.connection.readyState > 0) {
    console.log(`Server ready at http://localhost:${port}${graphqlServer.graphqlPath}`);
  }
});
