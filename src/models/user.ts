import mongoose, { Schema } from 'mongoose';
import { composeWithMongoose } from 'graphql-compose-mongoose';
import { CollectonTC } from './collection';

export const UserSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    unique: true,
    required: true,
  },
},
{
  collection: 'users',
});

export const User = mongoose.model('user', UserSchema);
export const UserTC = composeWithMongoose(User);

UserTC.addRelation('collection', {
  resolver: () => CollectonTC.getResolver('findMany'),
  prepareArgs: {
    userId: (source: any) => source._id || []
  },
  projection: { userId: true },
})