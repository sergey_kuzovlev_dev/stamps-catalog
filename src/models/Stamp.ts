import mongoose, { Schema } from 'mongoose';
import { composeWithMongoose } from 'graphql-compose-mongoose';
import { CurrencyTC } from './currency';

export const StampSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  issuedOn: {
    type: String,
    required: true,
  },
  catalogCode: {
    type: String,
    trim: true,
    required: true,
  },
  name: {
    type: String,
    trim: true,
    required: true,
  },
  series: {
    type: String,
    trim: true,
    required: true,
  },
  size: {
    type: String,
    trim: true,
    required: true,
  },
  faceValue: {
    type: Number,
    trim: true,
    required: true,
  },
  currencyId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  note: {
    type: String,
    trim: true,
    required: true,
  },
  image: {
    type: String,
    trim: true,
    required: true,
  }
},
{
  collection: 'stamps',
});

export const Stamp = mongoose.model('stamp', StampSchema);
export const StampTC = composeWithMongoose(Stamp);

StampTC.addRelation('currency', {
  resolver: () => CurrencyTC.getResolver('findById'),
  prepareArgs: {
    _id: (source: any) => source.currencyId || null
  },
  projection: { currencyId: true },
})