import mongoose, { Schema } from 'mongoose';
import { composeWithMongoose } from 'graphql-compose-mongoose';

export const CurrencySchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    trim: true,
    required: true,
  },
  sign: {
    type: String,
    trim: true,
    unique: true,
    required: true,
  },
  
},
{
  collection: 'currencies',
});

export const Currency = mongoose.model('currency', CurrencySchema);
export const CurrencyTC = composeWithMongoose(Currency);
