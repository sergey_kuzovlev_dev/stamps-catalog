import mongoose, { Schema } from 'mongoose';
import { composeWithMongoose } from 'graphql-compose-mongoose';
import { StampTC } from './stamp';

export const CollectionSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  stampId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
},
{
  collection: 'collections',
});

export const Collecton = mongoose.model('collecton', CollectionSchema);
export const CollectonTC = composeWithMongoose(Collecton);

CollectonTC.addRelation('stamp', {
  resolver: () => StampTC.getResolver('findById'),
  prepareArgs: {
    _id: (source: any) => source.stampId || null
  },
  projection: { stampId: true },
})